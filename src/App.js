import React, { Component } from 'react';
import './App.css';

const DisplayMode = Object.freeze({Episodes:1, Favorites:2});

class RickAndMorty extends  Component {
  constructor(props) {
    super(props);
    this.rootURL = "https://rickandmortyapi.com/api/";
    this.state = {
      episodes: [],
      characters: [],
      favorites: [],
      showInfo: -1,
    };
  }

  toggleFavorite(episodeID) {
    this.setState(previousState => {
        let newFavorites = [...previousState.favorites];
        const index = newFavorites.indexOf(episodeID);
        if (index === -1) {
          newFavorites.push(episodeID);
        } else {
          newFavorites.splice(index,1);
        }
        return { favorites: newFavorites }
      }
    )
  }

  toggleShowInfo(episodeID) {
    this.setState(previousState => {
        let newShowInfo;
        if ( previousState.showInfo === episodeID) {
          newShowInfo = -1;
        } else {
          newShowInfo = episodeID;
        }
        return { showInfo: newShowInfo }
      }
    )
  }

  fetchJSON(url) {
    return fetch(this.rootURL + url)
      .then(response => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response
      })
      .then(response => {
        return response.json();
      })
      .catch(error => {
        console.log(`fetchJSON URL:  + ${this.rootURL + url}`) ;
        console.log(`fetchJSON ERROR:  + ${error}`) ;
      });
  }

  static parseEpisode(episode) {
    const characterIDs = episode.characters.map(characterURL => {
      const stringID = characterURL.substring(characterURL.lastIndexOf('/')+1);
      return parseInt(stringID, 10);
    });

    const [seasonNumber, episodeNumber]  = episode.episode.match(/\d\d+/g);

    return {
      id: episode.id,
      name: episode.name,
      seasonNumber: "Season: " + parseInt(seasonNumber),
      episodeNumber:  "Episode: " + parseInt(episodeNumber),
      airDate: episode.air_date,
      characterIDs: characterIDs,
    }
  }

  static parseCharacter(character) {
    return {
      name: character.name,
      status: character.status,
      species: character.species,
      origin: character.origin.name,
      image: character.image,
    }
  }


  fetchEpisodesAndCharacters () {
    let firstPagePromise = this.fetchJSON("episode");
    let episodes = [];
    firstPagePromise
      .then(page => {
        const numberOfPages = page.info.pages;
        const remainingPagesPromises = [];
        for (let pageIndex = 2; pageIndex <= numberOfPages; pageIndex++) {
          remainingPagesPromises.push(this.fetchJSON(`episode/?page=${pageIndex}`));
        }

        const episodeList = page.results;
        episodeList.forEach(episode => {
          episodes[episode.id] = RickAndMorty.parseEpisode(episode);
        });

        Promise.all(remainingPagesPromises)
          .then(pageList => {
            pageList.forEach(page => {
              const episodeList = page.results;
              episodeList.forEach(episode => {
                episodes[episode.id] = RickAndMorty.parseEpisode(episode);
              });
            });

            this.setState(
              {episodes: episodes,},
              this.fetchCharacters());
          })
          .catch(error => {
            console.log(`ERROR:  + ${error}`);
          });
      })
      .catch(error => {
        console.log(`ERROR:  + ${error}`);
      });
  }

  fetchCharacters () {
    let firstPagePromise = this.fetchJSON("character");
    let characters = [];
    firstPagePromise
      .then(page => {
        const numberOfPages = page.info.pages;
        const remainingPagesPromises = [];
        for (let pageIndex = 2; pageIndex <= numberOfPages; pageIndex++) {
          remainingPagesPromises.push(this.fetchJSON(`character/?page=${pageIndex}`));
        }

        const characterList = page.results;
        characterList.forEach(character => {
          characters[character.id] = RickAndMorty.parseCharacter(character);
        });

        Promise.all(remainingPagesPromises).then(pageList => {
          pageList.forEach(page => {
            const characterList = page.results;
            characterList.forEach(character => {
              characters[character.id] = RickAndMorty.parseCharacter(character);
            });
          });

          this.setState({characters: characters});
        }).catch(error => {
          console.log(`ERROR:  + ${error}`);
        });
      })
      .catch(error => {
        console.log(`ERROR:  + ${error}`);
      });
  }

  componentDidMount() {
    this.fetchEpisodesAndCharacters();
  }

  render () {
    if (this.state.episodes.length < 1) {
      return (<div>Loading data</div>);
    } else {
      let episodesList = [];
      if (this.props.displayMode === DisplayMode.Episodes) {
        episodesList = this.state.episodes.map(episode => {
          return (
            <div key={episode.id}>
              <Episode episode={episode}
                       characters={this.state.characters}
                       onClickFavorite={() => this.toggleFavorite(episode.id)}
                       addToFavorites={this.state.favorites.indexOf(episode.id) === -1 }
                       onClickShowInfo={() => this.toggleShowInfo(episode.id)}
                       showInfo={this.state.showInfo === episode.id}/>
            </div>
          );
        });
      } else {
        this.state.episodes.forEach(episode => {
          if (this.state.favorites.indexOf(episode.id) !== -1) {
            episodesList.push(
              <div key={episode.id}>
                <Episode episode={episode}
                         characters={this.state.characters}
                         onClickFavorite={() => this.toggleFavorite(episode.id)}
                         addToFavorites={this.state.favorites.indexOf(episode.id) === -1}
                         onClickShowInfo={() => this.toggleShowInfo(episode.id)}
                         showInfo={this.state.showInfo === episode.id}/>
              </div>
            );
          }
        });
      }

      return (<div>{episodesList}</div>);
    }
  }
}

function EpisodeHeader(props) {
  return (
    <div className="episode-header-container">
      <div className="episode-header-name">
        {props.episode.name}
      </div>
      <div className="episode-header-favorites">
        <button className={"choice-" + props.addToFavorites.toString()}
                onClick={props.onClickFavorite}>
          {(props.addToFavorites) ? "Add to Favorites" : "Remove From Favorites"}
        </button>
      </div>
      <div className="episode-header-more-info">
        <button className={"choice-" + (!props.showInfo).toString()}
                onClick={props.onClickShowInfo}>
          {(!props.showInfo) ? "Show Info" : "Hide Info"}
        </button>
      </div>
    </div>
  );
}

function EpisodeInfo(props) {
  return (
    <div className="info-container">
      <div className="info-item"> {props.episode.seasonNumber}</div>
      <div className="info-item"> {props.episode.episodeNumber}</div>
      <div className="info-long-item"> {"Air Date - " + props.episode.airDate}</div>
    </div>
  );
}

function Character(props) {
  const character = props.character;
  return (
    <div className="info-container">
      <img className="info-image-item"  src={character.image} width={50} height={50} alt="failed to download"/>
      <div className="info-item"> {character.name}</div>
      <div className="info-item"> {character.status}</div>
      <div className="info-item"> {character.species}</div>
      <div className="info-item">{character.origin}</div>
    </div>
  );
}

function Episode(props) {
  if (props.characters.length > 0 && props.showInfo) {
    return (
      <div>
        <EpisodeHeader {...props}/>
        <EpisodeInfo episode={props.episode}/>
        {
          props.episode.characterIDs.map(id => {
            return (
              <div key={id}>
                <Character character={props.characters[id]}/>
              </div>);})
        }
      </div>
    );
  } else {
    return (<EpisodeHeader {...props}/>);
  }
}

class App extends Component {
  constructor (props) {
    super(props);
    this.state = {
      displayMode: DisplayMode.Episodes
    }
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Rick And Morty</h1>
          <div className="display-container">
            <button className="display-item" onClick={() => {
              this.setState( previousState => {
                if (previousState.displayMode === DisplayMode.Episodes) {
                  return {displayMode: DisplayMode.Favorites};
                } else {
                  return {displayMode: DisplayMode.Episodes};
                }
              })
            }}>
              {
                (this.state.displayMode === DisplayMode.Episodes) ?
                "Show Favorites" : "Show All Episodes"
              }
            </button>
          </div>
        </header>
        <RickAndMorty displayMode={this.state.displayMode}/>
      </div>
    );
  }
}

export default App;